#!/bin/bash
#
# This script Changes the Region (time zone and any related configs)

# Usage info
show_help() {
cat << EOF
Usage: ${0##*/} [-hv] [REGION]...
Change the Region to REGION by setting the timezone and other related configs.

    -h          display this help and exit
    -v          verbose mode
EOF
}

# Initialize our own variables:
verbose=0

# Resetting OPTIND is necessary if getopts was used previously in the script.
# It is a good idea to make OPTIND local if you process options in a function.
OPTIND=1

while getopts hv: opt; do
  case $opt in
    h)
      show_help
      exit 0
      ;;
    v)
      verbose=1
      ;;
    *)
      show_help >&2
      exit 1
      ;;
  esac
done
shift "$((OPTIND-1))" # Discard the options and sentinel --
# Everything that's left in "$@" is a non-option.  In our case, a REGION

case $@ in
  cu|CU)
    echo "Changing Region to Champaign-Urbana"
    echo "Setting timezone to America/Chicago..."
    timedatectl set-timezone America/Chicago
    echo "Ranking mirrorlist by connection and opening speeds..."
    sudo cp mirrorlist.cu /etc/pacman.d/mirrorlist
    echo "Changing Redshift lat/lon..."
    systemctl --user stop redshift-gtk
    systemctl --user start redshift-gtk
    echo "Reloading Sway..."
    swaymsg reload
    echo "Done"
    exit 0
    ;;
  sf|SF)
    echo "Changing Region to San Francisco"
    echo "Setting timezone to America/Los_Angeles..."
    timedatectl set-timezone America/Los_Angeles
    echo "Ranking mirrorlist by connection and opening speeds..."
    sudo cp mirrorlist.sf /etc/pacman.d/mirrorlist
    echo "Changing Redshift lat/lon..."
    systemctl --user stop redshift-gtk
    systemctl --user start redshift-gtk
    echo "Reloading Sway..."
    swaymsg reload
    echo "Done"
    exit 0
    ;;
  *)
    echo "$@ is not a supported region"
    show_help $@
    exit 1
    ;;
esac

# End of file
