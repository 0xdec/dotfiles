function update
    yay -Pw
    yay -Syu
    yarn global upgrade-interactive
    rustup toolchain install stable
end
