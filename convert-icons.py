import argparse, os, pystache
from fuzzysearch import find_near_matches
from fontTools.ttLib import TTFont

FONT_PATHS = ['~/.local/share/fonts', '/usr/share/fonts']

parser = argparse.ArgumentParser(description='Convert between icon names & glyphs.')
parser.add_argument('font', action='append',
                    help='icon font to use')
parser.add_argument('-r', '--reverse', action='store_true',
                    help='reverse conversion (convert glyphs to names)')
parser.add_argument('-i', '--input',
                    help='input file to be converted')
parser.add_argument('-o', '--output',
                    help='converted output filename')

args = parser.parse_args()

# Fuzzy find first instance of name in the given list of paths
def find(name, paths):
    for path in paths:
        for root, dirs, files in os.walk(path):
            for file in files:
                if find_near_matches(name, file, max_l_dist=1):
                    return os.path.join(root, file)

# Find all font files
fonts = []
for font_name in args.font:
    font_file = find(font_name, FONT_PATHS)
    cmap = TTFont(font_file)['cmap'].getBestCmap()
    imap = dict([(value, chr(key)) for key, value in cmap.items()])
    font = {
        'name': font_name,
        'cmap': cmap,
        'imap': imap
    }
    fonts.append(font)

# FIXME don't just use the first font
font = fonts[0]

if args.input:
    with open(args.input, 'r' if args.output else 'r+', encoding='utf-8') as f:
        rendered = pystache.render(f.read(), font['imap'])
        print(rendered)
        f.close()
        with open(args.output if args.output else args.input, 'w', encoding='utf-8') as f:
            f.write(rendered)
            f.close()
else:
    cheatsheet = ''
    for c in font['cmap']:
        line = chr(c) + ' ' + hex(c) + ' ' + font['cmap'][c]
        cheatsheet += line + '\n'
        print(line)
    if args.output:
        with open(args.output, 'w', encoding='utf-8') as f:
            f.write(cheatsheet)
            f.close()
