#!/bin/bash

# Get the current volume
volume=$( pulsemixer --get-volume | cut -d " " -f 1 )

# Change the icon for different volume levels
icon=""
if [[ $( pulsemixer --get-mute ) != "0" ]]; then
    icon=""
elif (( volume < 10 )); then
    icon=""
elif (( volume < 50 )); then
    icon=""
elif (( volume < 100 )); then
    icon=""
else
    icon=""
fi

# Display a volume notification
notify-send -a volume -t 2000 -R /tmp/notification "" "<b>$icon $volume%</b>"
