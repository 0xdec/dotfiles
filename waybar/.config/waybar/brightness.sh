#!/bin/bash

# Get the current brighness
brightness=$( light -G | cut -d . -f 1 )

# Change the icon for different brightness levels
icon=""
if (( brightness < 50 )); then
    icon=""
else
    icon="<b></b>"
fi

# Display a brightness notification
notify-send -a brightness -t 2000 -R /tmp/notification "" "$icon <b>$brightness%</b>"
