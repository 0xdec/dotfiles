#!/bin/bash
#
# This script is intended to install all necessary packages
#
# Note: It is also expected that you will have configured another user
# on the system with the appropriate groups and that you will run this
# script as that user using sudo
#
# Written and maintained by: Zach Brewer
# Adapted by: Jordi Pakey-Rodriguez
#

script_name="install_packages"

SCRIPT_DIR="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

EDITOR=nano

# Must not be run as root
if [ $(whoami) == "root" ]; then
  echo "Error: This script must not be run as root"
  exit 1
fi



# All of the packages that need to be installed
DEPENDENCIES="clang jdk-openjdk lm_sensors openssh pacman-contrib ruby rustup xdg-user-dirs yarn stow"
NETWORK="blueman bluez bluez-utils chrony iwd networkmanager-iwd network-manager-connection-editor nm-connection-editor openvpn"
AUDIO="paprefs pavucontrol playerctl pulseaudio pulseaudio-bluetooth pulsemixer"
FONTS="fontpreview inter-font ttf-fira-code ttf-fira-mono ttf-fira-sans ttf-hack ttf-iosevka ttf-liberation ttf-material-design-icons-desktop-git ttf-public-sans ttf-roboto"

WM="gammastep geoclue mako notify-send.sh wlroots sway swaylock swayidle waybar wofi"
THEME="nordic-theme-git nordic-polar-theme-git"
BROWSERS="firefox"
MUSIC="clementine easytag gst-plugins-base gst-plugins-good lollypop"
APPLICATIONS="audacity code freecad gimp imv inkscape keepassxc keeweb-desktop-bin kicad kicad-library kicad-library-3d kicad-library-digikey-git kicad-nightly kitty libreoffice-fresh minecraft-launcher nemo nemo-fileroller nemo-preview nemo-python okular openscad signal-desktop syncthing-gtk teensyduino telegram-desktop-bin transmission-gtk vlc"

ANDROID="android-tools android-udev"
ARM="arm-none-eabi-gcc arm-none-eabi-gdb arm-none-eabi-newlib dfu-programmer dfu-util"
AVR="avr-gcc avr-gdb avr-libc avrdude"
PIC="microchip-mplabx-bin microchip-pic32-legacy-plib"

EXTRAS="bash-pipes cowsay espeak figlet file-roller fish fortune-mod fwupd grim htop i2c-tools mlocate nano-syntax-highlighting ncmatrix neofetch nftables nvme-cli powertop prettyping qdirstat s-tui sl slurp smartmontools solaar systemd-boot-pacman-hook thefuck toilet tree unimatrix-git wl-clipboard"

LAPTOP="intel-undervolt light x86_energy_perf_policy"


# Install yay (Pacman wrapper and AUR helper)
function installYay() {
  cd $HOME
  git clone https://aur.archlinux.org/yay.git
  cd yay
  makepkg -si
  cd $HOME
  rm -rf $HOME/yay
}

# Install Zsh (Z shell) and ZIM (Zsh IMproved FrameWork)
function installZshZim() {
  # Install zsh and zsh-completions
  yay -S zsh zsh-completions

  # Install zim
  git clone --recursive https://github.com/zimfw/zimfw.git ${ZDOTDIR:-${HOME}}/.zim

  chsh -s /bin/zsh

  source ${ZDOTDIR:-${HOME}}/.zlogin
}

# Install all other packages
function installPackages() {
  yay -Syu
  yay -S $DEPENDENCIES
  yay -S $NETWORK
  yay -S $AUDIO
  yay -S $FONTS
  yay -S $WM
  yay -S $THEME
  yay -S $BROWSERS
  yay -S $APPLICATIONS
  yay -S $ANDROID
  yay -S $ARM
  yay -S $AVR
  # yay -S $PIC
  yay -S $EXTRAS

  rustup toolchain install stable
}

function createDefaultDirectories() {
  xdg-user-dirs-update
}

function detectSensors() {
  (while :; do echo ""; done ) | sensors-detect
}



installYay
installZshZim
installPackages
detectSensors

echo ""
echo "All done!"
