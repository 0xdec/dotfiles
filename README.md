# dotfiles

My Arch Linux dotfiles and install scripts.

## Installation

Clone this repository:
```sh
$ git clone git@gitlab.com:jordiorlando/dotfiles.git ~/.dotfiles
$ cd ~/.dotfiles
```

Install zsh, yay, and other packages:
```sh
# Make the install script executable
$ chmod u+x install.sh
# Run the install script
$ ./install.sh
```

Use [GNU Stow](https://www.gnu.org/software/stow) to link dotfiles:
```sh
$ stow kitty
$ stow fish
$ stow gammastep
$ stow git
$ stow kicad
$ stow mako
$ stow nano
$ stow neofetch
$ stow npm
$ stow ssh
$ stow sway
$ stow waybar
$ stow wofi
$ stow yay
$ stow zsh
```
