#!/bin/bash
#
# This script changes the GUI/GTK Theme

case $1 in
    day)
        gsettings set org.gnome.desktop.interface gtk-theme "Nordic-Polar"
        gsettings set org.gnome.desktop.wm.preferences theme "Nordic-Polar"
        echo "system theme changed to day"
        ;;
    night)
        gsettings set org.gnome.desktop.interface gtk-theme "Nordic"
        gsettings set org.gnome.desktop.wm.preferences theme "Nordic"
        echo "system theme changed to night"
        ;;
esac
