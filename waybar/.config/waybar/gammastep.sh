#!/bin/bash

# Pipe location and icon
pipe=/tmp/gammasteppipe
icon_on="󰌵"
icon_off="󰌶"

# Different behavior if 'toggle' command is passed
if [[ "$1" == "toggle" ]]; then
    # Only write to the pipe if it exists
    if [[ ! -p $pipe ]]; then
        exit 1
    fi

    # Toggle gammastep
    if [[ $(systemctl --user is-active gammastep) == "active" ]]; then
        echo "inactive" > $pipe
        systemctl --user stop gammastep
    else
        echo "active" > $pipe
        systemctl --user start gammastep
    fi
else
    # Kill previous instances of this script
    script_name=${BASH_SOURCE[0]}
    for pid in $(pidof -x $script_name); do
        if [[ $pid != $$ ]]; then
            kill -9 $pid
        fi
    done

    # Open a named pipe
    trap "rm -f $pipe" EXIT
    if [[ ! -p $pipe ]]; then
        mkfifo $pipe
    fi

    # Make sure gammastep is active on startup
    if [[ $(systemctl --user is-active gammastep) == "inactive" ]]; then
        systemctl --user start gammastep
    fi
    echo "$icon_on"

    # Read the pipe and update waybar whenever the gammastep state changes
    while true
    do
        if read line <$pipe; then
            if [[ "$line" == "active" ]]; then
                #notify-send -a gammastep -c on -t 3000 -R /tmp/notification "$icon_on"
                echo "$icon_on"
            else
                #notify-send -a gammastep -c off -t 3000 -R /tmp/notification "$icon_off"
                echo "$icon_off"
            fi
        fi
    done
fi
