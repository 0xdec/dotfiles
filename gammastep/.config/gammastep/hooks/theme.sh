#!/bin/sh

if "$1" = period-changed; then
    case $3 in
        night)
            gsettings set org.gnome.desktop.interface gtk-theme "Nordic"
            gsettings set org.gnome.desktop.wm.preferences theme "Nordic"
            ;;
        transition)
            ;;
        daytime)
            gsettings set org.gnome.desktop.interface gtk-theme "Nordic-Polar"
            gsettings set org.gnome.desktop.wm.preferences theme "Nordic-Polar"
            ;;
    esac
fi
